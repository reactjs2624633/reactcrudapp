import React, { useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";

const Edit = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    username: "",
    email: "",
    city: "",
    phone: "",
  });

  useEffect(() => {
    const dataFromStorage = JSON.parse(localStorage.getItem("userData")) || [];
    const userToUpdate = dataFromStorage.find(
      (user) => user.id === parseInt(id)
    );
    if (userToUpdate) {
      setFormData(userToUpdate);
    }
  }, [id]);

  const handleChange = (e) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const dataFromStorage = JSON.parse(localStorage.getItem("userData")) || [];
    const updatedUserData = dataFromStorage.map((user) =>
      user.id === parseInt(id) ? formData : user
    );

    localStorage.setItem("userData", JSON.stringify(updatedUserData));
    navigate("/");
  };

  return (
    <>
      <Link to="/">Back</Link>

      <form className="d-flex flex-column gap-3 mt-3" onSubmit={handleSubmit}>
        <div className="fields">
          <label htmlFor="username">Name</label>
          <input
            type="text"
            id="username"
            name="username"
            value={formData.username}
            onChange={handleChange}
          />
        </div>

        <div className="fields">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
          />
        </div>

        <div className="fields">
          <label htmlFor="city">City</label>
          <input
            type="text"
            id="city"
            name="city"
            value={formData.city}
            onChange={handleChange}
          />
        </div>

        <div className="fields">
          <label htmlFor="phone">Mobile</label>
          <input
            type="text"
            id="phone"
            name="phone"
            value={formData.phone}
            onChange={handleChange}
          />
        </div>

        <div>
          <button className="btn btn-info px-5 py-2" type="submit">
            Update
          </button>
        </div>
      </form>
    </>
  );
};

export default Edit;
